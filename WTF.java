import java.util.Scanner;

public class WTF {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int Q = scanner.nextInt();

        for (int q = 0; q < Q; q++) {
            int N = scanner.nextInt();
            double[][] points = new double[N][2];

            for (int i = 0; i < N; i++) {
                points[i][0] = scanner.nextDouble();
                points[i][1] = scanner.nextDouble();
            }

            double minDistance = findClosestDistance(points);
            if (minDistance <= 10000) {
                System.out.printf("%.1f\n", minDistance);
            } else {
                System.out.println("No answer");
            }
        }
    }

    private static double findClosestDistance(double[][] points) {
        double minDistance = Double.MAX_VALUE;

        for (int i = 0; i < points.length - 1; i++) {
            for (int j = i + 1; j < points.length; j++) {
                double distance = calculateDistance(points[i][0], points[i][1], points[j][0], points[j][1]);
                minDistance = Math.min(minDistance, distance);
            }
        }

        return minDistance;
    }

    private static double calculateDistance(double x1, double y1, double x2, double y2) {
        return Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
    }
}
