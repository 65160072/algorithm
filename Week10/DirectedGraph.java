import java.util.*;

public class DirectedGraph {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // อ่านจำนวนโหนดและจำนวนเส้นเชื่อม
        int numNodes = scanner.nextInt();
        int numEdges = scanner.nextInt();

        // สร้าง adjacency list ด้วย HashMap
        Map<Integer, List<Integer>> adjacencyList = new HashMap<>();

        // เพิ่มโหนดเข้าไปใน adjacency list
        for (int i = 1; i <= numNodes; i++) {
            adjacencyList.put(i, new ArrayList<>());
        }

        // เพิ่มเส้นเชื่อมลงใน adjacency list
        for (int i = 0; i < numEdges; i++) {
            int source = scanner.nextInt();
            int destination = scanner.nextInt();
            adjacencyList.get(source).add(destination);
        }

        // พิมพ์ adjacency list ออกมา
        for (int node : adjacencyList.keySet()) {
            System.out.print(node + " -> ");
            List<Integer> neighbors = adjacencyList.get(node);
            if (neighbors.isEmpty()) {
                System.out.println("None");
            } else {
                for (int neighbor : neighbors) {
                    System.out.print(neighbor + " ");
                }
                System.out.println();
            }
        }
    }
}
