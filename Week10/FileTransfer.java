import java.util.*;

public class FileTransfer {
    static boolean[][] graph;
    static boolean[] visited;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt(); // จำนวนเครื่องคอมพิวเตอร์
        int M = scanner.nextInt(); // จำนวนสายเชื่อมต่อ
        int Q = scanner.nextInt(); // จำนวนคำถาม

        graph = new boolean[N + 1][N + 1];
        visited = new boolean[N + 1];

        // อ่านข้อมูลการเชื่อมต่อระหว่างเครื่องคอมพิวเตอร์
        for (int i = 0; i < M; i++) {
            int S = scanner.nextInt();
            int E = scanner.nextInt();
            graph[S][E] = true;
        }

        // ตรวจสอบคำถาม
        for (int i = 0; i < Q; i++) {
            int T = scanner.nextInt();
            int U = scanner.nextInt();
            Arrays.fill(visited, false); // เคลียร์ visited array ใหม่ก่อนการค้นหาทุกครั้ง
            if (dfs(T, U)) {
                System.out.println("Yes");
            } else {
                System.out.println("No");
            }
        }
    }

    // ฟังก์ชั่น DFS ในการค้นหาว่ามีเส้นทางเชื่อมจากเครื่อง T ไปยังเครื่อง U หรือไม่
    static boolean dfs(int current, int target) {
        if (current == target) {
            return true;
        }
        visited[current] = true;
        for (int i = 1; i < graph.length; i++) {
            if (graph[current][i] && !visited[i]) {
                if (dfs(i, target)) {
                    return true;
                }
            }
        }
        return false;
    }
}
