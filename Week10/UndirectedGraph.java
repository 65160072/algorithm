import java.util.*;

public class UndirectedGraph {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // อ่านจำนวนโหนดและจำนวนเส้นเชื่อม
        int numNodes = scanner.nextInt();
        int numEdges = scanner.nextInt();

        // สร้าง adjacency list ด้วย HashMap
        Map<Integer, List<Integer>> adjacencyList = new HashMap<>();

        // เพิ่มโหนดเข้าไปใน adjacency list
        for (int i = 1; i <= numNodes; i++) {
            adjacencyList.put(i, new ArrayList<>());
        }

        // เพิ่มเส้นเชื่อมลงใน adjacency list
        for (int i = 0; i < numEdges; i++) {
            int node1 = scanner.nextInt();
            int node2 = scanner.nextInt();
            // เพิ่มเส้นเชื่อมในทิศทางทั้งสองทิศ
            adjacencyList.get(node1).add(node2);
            adjacencyList.get(node2).add(node1);
        }

        // พิมพ์ adjacency list ออกมา
        for (int node : adjacencyList.keySet()) {
            System.out.print(node + " -> ");
            List<Integer> neighbors = adjacencyList.get(node);
            Collections.sort(neighbors);
            for (int i = 0; i < neighbors.size(); i++) {
                System.out.print(neighbors.get(i));
                if (i != neighbors.size() - 1) {
                    System.out.print(", ");
                }
            }
            System.out.println();
        }
    }
}
