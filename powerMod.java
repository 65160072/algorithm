import java.util.Scanner;
/**
 * powerMod
 */
public class powerMod {
    public static int cal(int a,int b, int c){
        if(b == 0){
            return 1 ;
        }else{
            int result = cal(a,b/2,c);
            if(b%2==0){
                return (result*result) % c ;
            }else{
                return (a*result*result) % c;
            }
        } 
        
    }
    public static void main(String[] args) {
        int a = 0;
        int b = 0;
        int c = 0;
        Scanner kb = new Scanner(System.in);
        int Q = kb.nextInt(); 
        if(Q>10){
            return ;
        }else{
            for(int i =0;i<Q ;i++){
                a = kb.nextInt();
                b = kb.nextInt();
                c = kb.nextInt();
                int result = cal(a, b, c);
                System.out.println(result);
            }
        }
        
    }
}

