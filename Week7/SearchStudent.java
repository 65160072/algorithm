import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;
public class SearchStudent {
    private int id;
    private String name;

    public SearchStudent(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public static SearchStudent binarySearchById(SearchStudent[] students, int key) {
        int low = 0;
        int high = students.length - 1;
        SearchStudent student = null;

        while (low <= high) {
            int mid = (low + high) / 2;
            if (students[mid].getId() < key) {
                low = mid + 1;
            } else if (students[mid].getId() > key) {
                high = mid - 1;
            } else if (students[mid].getId() == key) {
                student = students[mid];
                System.out.println("found");
                break;
            }
        }
        if(student==null){
            System.out.println("not found");
        }
        return student;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        SearchStudent[] studentArray = new SearchStudent[n];

        // set student array value
        for (int i = 0; i < studentArray.length; i++) {
            studentArray[i] = new SearchStudent(input.nextInt(), input.next());
        }

        // sort array

        // display student array
        for (int i = 0; i < n; i++) {
            System.out.println(studentArray[i]);
        }

        // get type to search
        String typeSearch = input.next();
        if (typeSearch.equals("id")) {
            Arrays.sort(studentArray, Comparator.comparingInt(SearchStudent::getId));
            int id = input.nextInt();
            binarySearchById(studentArray, id);
        } else if (typeSearch.equals("name")) {
            Arrays.sort(studentArray, Comparator.comparing(SearchStudent::getName));
            String name = input.next();
            SearchStudent keyStudent = new SearchStudent(0, name); // Create a dummy student for comparison
            int index = Arrays.binarySearch(studentArray, keyStudent, Comparator.comparing(SearchStudent::getName));
            if (index >= 0) {
                System.out.println("found");
            } else {
                System.out.println("not found");
            }
        }
    }
}