import java.util.Arrays;
import java.util.Scanner;

public class CoinChange {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numCoins = scanner.nextInt();
        int targetAmount = scanner.nextInt();
        int[] coinValues = new int[numCoins];
        for (int i = 0; i < numCoins; i++) {
            coinValues[i] = scanner.nextInt();
        }
        int result = minimumCoinsRequired(coinValues, targetAmount);
        System.out.println(result);
        scanner.close();
    }
    private static int minimumCoinsRequired(int[] coinValues, int targetAmount) {
        int[] dp = new int[targetAmount + 1];
        Arrays.fill(dp, Integer.MAX_VALUE);
        dp[0] = 0;

        for (int coinValue : coinValues) {
            for (int i = coinValue; i <= targetAmount; i++) {
                if (dp[i - coinValue] != Integer.MAX_VALUE) {
                    dp[i] = Math.min(dp[i], dp[i - coinValue] + 1);
                }
            }
        }

        return dp[targetAmount] == Integer.MAX_VALUE ? 0 : dp[targetAmount];
    }
}
