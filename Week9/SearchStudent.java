import java.util.*;

class Student {
    int id;
    String name;
    double gpa;

    public Student(int id, String name, double gpa) {
        this.id = id;
        this.name = name;
        this.gpa = gpa;
    }
}

class SortBy implements Comparator<Student> {
    String sortBy;

    public SortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public int compare(Student a, Student b) {
        switch (sortBy) {
            case "id":
                return a.id - b.id;
            case "name":
                return a.name.compareTo(b.name);
            case "gpa":
                if (a.gpa < b.gpa)
                    return -1;
                if (a.gpa > b.gpa)
                    return 1;
                return 0;
            default:
                return 0;
        }
    }
}

public class SearchStudent {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        while (scanner.hasNext()) {
            int n = scanner.nextInt();
            String order = scanner.next();
            String sortBy = scanner.next();

            List<Student> students = new ArrayList<>();

            for (int i = 0; i < n; i++) {
                int id = scanner.nextInt();
                String name = scanner.next();
                double gpa = scanner.nextDouble();

                students.add(new Student(id, name, gpa));
            }

            Collections.sort(students, new SortBy(sortBy));

            if (order.equals("d")) {
                Collections.reverse(students);
            }

            for (Student student : students) {
                System.out.println(student.id + " " + student.name + " " + student.gpa);
            }
        }

        scanner.close();
    }
}